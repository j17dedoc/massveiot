import BME280
import json
import COAP as coap
from network import LoRa
import socket
import time
import binascii
import pycom
from network import WLAN
from network import Bluetooth
import cbor
import struct




from machine import I2C


def send_data(data):
    c=cbor.dumps(data)
    global s
    try:
        s.send(struct.pack(str(c.length()*2)+'s',binascii.hexlify(c.value())))
    except Exception as e:
        print(e)


wlan = WLAN(mode=WLAN.STA)
bt = Bluetooth()


i2c = I2C(0, I2C.MASTER, baudrate=400000)
print (i2c.scan())

bme=BME280.BME280(i2c=i2c)



lora = LoRa(mode=LoRa.LORAWAN)

print ("devEUI {}".format(binascii.hexlify(lora.mac())))

app_eui = binascii.unhexlify('00 00 00 00 00 00 00 00'.replace(' ',''))
app_key = binascii.unhexlify('11 22 33 44 55 66 77 88 11 22 33 44 55 66 77 88'.replace(' ',''))
lora.join(activation=LoRa.OTAA, auth=(app_eui, app_key),  timeout=0)

pycom.heartbeat(False)
pycom.rgbled(0xFFFFFF)


while not lora.has_joined():
    time.sleep(2.5)
    print('Not yet joined...')
pycom.rgbled(0x001100)
print('Joined')

s = socket.socket(socket.AF_LORA, socket.SOCK_RAW)
#s.bind(5)
s.setsockopt(socket.SOL_LORA, socket.SO_DR, 5)
s.setsockopt(socket.SOL_LORA,  socket.SO_CONFIRMED,  False)

packet=0

while True:
    print("Data Series number ",packet)
    pycom.rgbled(0xFF0000)
    wifis = []
    nets = wlan.scan()
    for net in nets:
        #wifis.append([ net.bssid,net.rssi])
        #bssid=struct.unpack(len(net.bssid)+'s',net.bssid)
        bssid=[]
        for digit in net.bssid:
            bssid.append(digit)
        wifis.append(bssid)
        wifis.append(net.rssi)
    bluetooths = []
    pycom.rgbled(0x0000FF)
    bt.start_scan(2)
    t0 = time.time()
    adv = None
    macs=[]
    while time.time() - t0 < 1:
        adv = bt.get_adv()
        if adv:
            if adv.mac not in macs:
                mac=[]
                macs.append(adv.mac)
                for digit in adv.mac:
                    mac.append(digit)
                bluetooths.append(mac)
                bluetooths.append(adv.rssi)

    temp = bme.read_temperature()
    pres = bme.read_pressure()
    humi = bme.read_humidity()

    data_tph = [0,packet,temp,pres,humi]
    data_wifi=[[1,packet]]
    data_bluetooth=[[2,packet]]


    count=0
    for wifi in wifis:
        if count>9:
            data_wifi.append([1,packet])
            count=0
        data_wifi[-1].append(wifi)
        count+=1

    count=0
    for bluetooth in bluetooths:
        if count>9:
            data_bluetooth.append([2,packet])
            count=0
        data_bluetooth[-1].append(bluetooth)
        count+=1

    pycom.rgbled(0xFFFF00)

    send_data(data_tph)
    print('Sent TPH Data')
    time.sleep (5)
    pycom.rgbled(0xFF0000)
    for wifi in data_wifi:
        send_data(wifi)
        print('Sent WiFi Data')
        time.sleep (5)
    pycom.rgbled(0x0000FF)
    for bluetooth in data_bluetooth:
         send_data(bluetooth)
         print('Sent Bluetooth Data')
         time.sleep (5)

    packet+=1
    print('Sleep')
    pycom.rgbled(0x000000)
    s.setblocking(False)
    time.sleep (60)