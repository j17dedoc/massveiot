import pickle
from pymongo import MongoClient

MONGO_HOST = "127.0.0.1"
MONGO_PORT = 27017
MONGO_DB = "projets517"

def connect(collection):
    try:
        client = MongoClient(MONGO_HOST, MONGO_PORT)
        db = client[MONGO_DB]
        return db[collection]
    except Exception as e:
        print(e)
        return False

def get_all_collections():
    client = MongoClient(MONGO_HOST, MONGO_PORT)
    db = client[MONGO_DB]
    return db.collection_names()

def get_whole_collection(collection):
    collection_ = connect(collection)
    return collection_.find()

def get_all_documents():
    list_ = []
    for col in get_all_collections():
        for doc in get_whole_collection(col):
            list_.append(doc)
    return list_

if __name__ == "__main__":
    with open('entire_base.pickle', 'wb') as f:
        pickle.dump(get_all_documents(), f)
