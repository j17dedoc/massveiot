from logging import error
from pymongo import MongoClient
from bson.objectid import ObjectId

MONGO_HOST = "172.17.0.2"
MONGO_PORT = 27017
MONGO_DB = "projets5"

def connect(collection):
    """Connects to a mongo connection in MONGO_DB
    
    Arguments:
        collection {str} -- The mongo collection to connect on
    Returns:
        [collection object] -- The connection object 
    """
    try:
        client = MongoClient(MONGO_HOST, MONGO_PORT)
        db = client[MONGO_DB]
        return db[collection]
    except Exception as e:
        error(e)
        return False
    
def insert_json(collection, json_to_add):
    """Insert a JSON (dict) to a specific mongo collection
    
    Arguments:
        collection {str} -- Collection to insert the json into
        json_to_add {dict} -- Object to insert into mongo
    """
    try:
        json_to_add = dict(json_to_add)
        collection = connect(collection)
        id = collection.insert_one(json_to_add).inserted_id
        return id
    except Exception as e:
        error(e)
        return False

def update_field(collection, id, key_value):
    """Updates a document setting some fields
    
    Arguments:
        collection {collection object} -- The collection we want to update 
        id {str} -- The id of the document to update
        key_value {dict} -- The key-value's we want to add
    """
    try:
        collection = connect(collection) 
        result = collection.update_one({'_id':ObjectId(id)}, {'$set': key_value})
        return result
    except Exception as e:
        error(e)
        return False

def update_append_list(collection, id, field, list_):
    """Updates a document appending elements to a list
    
    Arguments:
        collection {collection object} -- The collection we want to update 
        id {str} -- The id of the document to update
        field {str} -- TThe field we want to update
        list_ {list} -- The list of elements we want to append to the original list
    """
    try:
        collection = connect(collection) 
        result = collection.update_one({'_id':ObjectId(id)}, {'$push': {field: {'$each': list_}}})
        return result
    except Exception as e:
        error(e)
        return False