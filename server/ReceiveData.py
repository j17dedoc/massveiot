from flask import Flask
from flask import request
from flask import Response
import datetime
import base64
import pprint
import json
import struct
import cbor2 as cbor
import binascii
import insertion as ins

app = Flask(__name__)
app.debug = True
global last_received
last_received={}

@app.route('/lns', methods=['POST'])
def get_from_LNS():
    global last_received

    fromGW = request.get_json(force=True)
    #print ("HTTP POST RECEIVED")
    pprint.pprint(fromGW)
    if "data" in fromGW:
        payload = base64.b64decode(fromGW["data"])
        result=struct.unpack(str(len(payload))+'s',payload)
        t=cbor.loads(binascii.unhexlify(result[0]))
        #print(binascii.unhexlify(result[0]))
        #t=cbor.loads(result[0])
        if "devEUI" in fromGW:
            if fromGW["devEUI"] not in last_received:
                last_received[fromGW["devEUI"]]=[t[1], None, 0, 0]
        print(t)
        json_to_store = {}
        if t[0] == 0:
            # TPH
            json_to_store['temperature'] = t[2]
            json_to_store['pression'] = t[3]
            json_to_store['humidite'] = t[4]
            json_to_store['deveui'] = fromGW['devEUI']
            json_to_store['lorasnr'] = fromGW['loRaSNR']
            json_to_store['lorarssi'] = fromGW['rssi']
            json_to_store['time'] = datetime.datetime.now()
            json_to_store['wifi'] = []
            json_to_store['bluetooth'] = []
            # insert into mongo and store the id
            id_mongo = ins.insert_json(fromGW['devEUI'], json_to_store)
            last_received[fromGW['devEUI']][1] = id_mongo
            last_received[fromGW['devEUI']][0]=t[1]
            last_received[fromGW['devEUI']][2]=0
            last_received[fromGW['devEUI']][3]=0


            print("Last received :",last_received)
        elif t[0] == 1:
            # Wifi
            wifi_temp = []
            for i in range(2,len(t),2):
                wifi_temp.append({"bssid":str(t[i]), "rssi": t[i+1]})
            result = ins.update_append_list(fromGW['devEUI'], last_received[fromGW['devEUI']][1], "wifi", wifi_temp)
            if result:
                print("Good")
        elif t[0] == 2:
            # Bluetooth
            bluetooth_temp = []
            for i in range(2,len(t),2):
                bluetooth_temp.append({"mac":str(t[i]), "rssi": t[i+1]})
            result = ins.update_append_list(fromGW['devEUI'], last_received[fromGW['devEUI']][1], "bluetooth", bluetooth_temp)
            if result:
                print("Good")
        return ''

if __name__ == '__main__':
    #print (sys.argv)

    defPort=2503
    try:
        opts, args = getopt.getopt(sys.argv[1:],"hp:",["port="])
    except getopt.GetoptError:
        #print ("{0} -p <port> -h".format(sys.argv[0]))
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            #print ("{0} -p <port> -h".format(sys.argv[0]))
            sys.exit()
        elif opt in ("-p", "--port"):
            defPort = int(arg)


    app.run(host="0.0.0.0", port=defPort)